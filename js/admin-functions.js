(function($){
    $('.slider_range').change(function () {
        $('.text_radius').val(
            function () {
                return $('.slider_range').val();
            }
        );
    });

    $('.text_radius').change(function (){
        $('.slider_range').val(
            function (){
                return $('.text_radius').val();
            }
        )
    });

    var positionTweets = {lat: parseFloat(send_settings.latitude), lng: parseFloat(send_settings.longitude)}
    var radiusTweets = parseFloat(send_settings.radius);
    var map, marker, tweetsCircle;

    $('.map').ready(function () {
        if( !isNaN(positionTweets.lat) && !isNaN(positionTweets.lng) ) {
            map = new google.maps.Map($('.map')[0], {
                zoom: 8,
                center: positionTweets,
            });

            marker = new google.maps.Marker({
                position: positionTweets,
                map: map
            });

            tweetsCircle = new google.maps.Circle({
                strokeColor: '#3c763d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#3c763d',
                fillOpacity: 0.30,
                map: map,
                center: positionTweets,
                radius: radiusTweets*1000,
            });
        }
        else{
            map = new google.maps.Map($('.map')[0], {
                zoom: 8,
                center: {lat: 47.800, lng: 35.005},
            });

        }

        $('.slider_range').change(function (event){
            if(typeof tweetsCircle !== 'undefined'){
                tweetsCircle.setMap(null);
            }

            if(typeof marker !== 'undefined'){
                tweetsCircle = new google.maps.Circle({
                    strokeColor: '#3c763d',
                    strokeOpacity: 0.9,
                    strokeWeight: 2,
                    fillColor: '#3c763d',
                    fillOpacity: 0.30,
                    map: map,
                    center: marker.position,
                    radius: parseFloat($(this).val())*1000,
                });
            }
        });

        google.maps.event.addListener(map, 'click', function(event) {

           if(typeof marker !== 'undefined'){
                marker.setMap(null);
                tweetsCircle.setMap(null);
            }

            marker = new google.maps.Marker({
                position: event.latLng,
                map: map
            });

            tweetsCircle = new google.maps.Circle({
                strokeColor: '#3c763d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#3c763d',
                fillOpacity: 0.30,
                map: map,
                center: event.latLng,
                radius: radiusTweets*1000,
            });
            var latitude = event.latLng.lat().toFixed(3);
            var longitude = event.latLng.lng().toFixed(3);
            $('.latitude').val(function () {
                return latitude;
            });
            $('.longitude').val(function () {
                return longitude;
            });

        });
    });

})(jQuery);



