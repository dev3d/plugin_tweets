<?php
/*
    Plugin Name: Tweets plugin
    Description: Some tweets plugin.
*/

require "vendor/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth as Twitter;
use Carbon\Carbon;
define('CONSUMER_KEY', 	'jFrap833qvwgiMpD37aYx0HsB');
define('CONSUMER_SECRET', '2TiebQLA8vCMoKvWxojHwpVfUjw1ObJt07Fe9sz5QHFuz9RUfd');




class TweetsPlugin
{
    public function __construct()
    {

        $this->TwitterAuthorization();
        add_action( 'admin_enqueue_scripts', [$this, 'init_admin_scripts']);
        add_action( 'wp_enqueue_scripts', [$this, 'init_scripts']);
        add_action('admin_init', [$this, 'addSettingsSection']);
        add_action('admin_menu', [$this, 'addSettingsPage']);
        register_activation_hook( __FILE__, [$this, 'createTable']);
        register_deactivation_hook(__FILE__, [$this, 'dropTable']);
        add_action('loadNewTweets', [$this, 'TwitterAuthorization']);
        add_shortcode( 'tweets', [$this,'shortcode_func'] );

        if( ! wp_next_scheduled( 'loadNewTweets' ) ) {
            wp_schedule_event( time(), 'hourly', 'loadNewTweets');
        }
    }

    public function init_admin_scripts() {

        wp_enqueue_style( 'style_plugin', plugins_url().'/plugin_tweets/css/admin-style.css' , array());
        wp_enqueue_script( 'script_plugin_tweets', plugins_url() . '/plugin_tweets/js/admin-functions.js', array('jquery', 'backbone'), '1.0.0', true );
        wp_enqueue_script( 'script_plugin_tweets_gMap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAuVbi5j_3omYFKTcBiUIROUan5H2UTUa0', array('jquery', 'backbone'), '1.0.0', true );
        $settings_tweets = array(
            'theme' => get_option( 'tweets_theme'  ),
            'latitude' => get_option( 'tweets_latitude' ),
            'longitude' => get_option( 'tweets_longitude' ),
            'radius' => get_option( 'tweets_radius_text' ),
        );
        wp_localize_script( 'script_plugin_tweets', 'send_settings', $settings_tweets );
    }

    public function init_scripts(){
        wp_enqueue_style( 'style_plugin_tweets', plugins_url().'/plugin_tweets/css/style.css' , array());
    }

    public function TwitterAuthorization(){
        $connection = new Twitter(CONSUMER_KEY, CONSUMER_SECRET);
        $t = $connection->get("search/tweets", array(
            "since_id" => get_option( 'max_id_tweets'),
            "q" => get_option( 'tweets_theme'  ),
            "geocode" => get_option( 'tweets_latitude' ).",".get_option( 'tweets_longitude' ).",".get_option( 'tweets_radius_text' )."km"));

        update_option( 'max_id_tweets', $value=$t->search_metadata->max_id_str);

        global $wpdb;
        foreach ($t->statuses as $item) {
            //echo'<pre>'; var_dump($item);
            $wpdb->insert('wp_tweets' , array(
                'id_tweet'=>$item->user->id,
                'text' =>$item->text,
                'created_add_data' => Carbon::parse($item->created_at)->toDateTimeString(),
                'user_name' =>$item->user->screen_name,
                'profile_img_url' => $item->user->profile_image_url,
                'location' => $item->user->location,
            ));
        }
        //die;
    }

    public function createTable(){

        global $wpdb;
        $table_name = $wpdb->prefix . 'tweets';
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $sql = "CREATE TABLE {$table_name} (
        id int(11) unsigned NOT NULL auto_increment,
        id_tweet int(50) unsigned,
        text VARCHAR (250) NOT NULL default '',
        created_add_data DATETIME,
        user_name VARCHAR(100) NOT NULL default '',
        profile_img_url VARCHAR (250) NOT NULL default '',
        location VARCHAR (50) NOT NULL default '',
        PRIMARY KEY  (id))";
        dbDelta( $sql );
    }

    public function dropTable(){
        global $wpdb;
        $table_name = $wpdb->prefix . 'tweets';
        $sql = "DROP TABLE IF EXISTS $table_name;";
        $wpdb->query($sql);
    }

    function shortcode_func( $atts ) {
        global $wpdb;
        $tweets = $wpdb->get_results("SELECT * FROM (SELECT * FROM wp_tweets ORDER BY id DESC LIMIT 10) AS T ORDER BY id ASC");
?>
        <?php foreach ($tweets as $item):?>
            <div class="tweet">
                <div class="media-left media-middle">
                    <img class="media-object profile_img" src="<?php echo $item->profile_img_url;?>" alt="Profile_image">
                </div>
                <div class="media-body">
                    <p  class="text"><?php echo  $item->text;?></p>
                </div>
                <p  class="user_name"><?php echo $item->user_name;?></p>
                <p class="add_data"><?php echo Carbon::parse($item->created_add_data)->diffForHumans();?></p>
                <p class="location"><?php echo $item->location;?></p>
            </div>
        <?php endforeach; ?>
<?php

    }


    public function addSettingsPage(){
        add_options_page('Tweets Settings', 'Tweets Settings', 'manage_options', 'tweets-settings', [$this, 'renderSettingsPage']);
    }

    public function renderSettingsPage(){
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }


        ?>
        <div class="wrap">

            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <form action="options.php" method="post">
                <?php
                // output security fields for the registered setting "wporg"
                settings_fields( 'tweets_group' );
                // output setting sections and their fields
                // (sections are registered for "wporg", each field is registered to a specific section)
                do_settings_sections( 'tweets_section' );
                // output save settings button
                submit_button( 'Save Settings' );
                ?>
            </form>
            <div class="map"></div>
        </div>
        <?php
    }

    public function addSettingsSection(){
        add_settings_section(
            'tweets_section',
            'Tweets Settings',
            [$this, 'renderSettingsSection'],
            'tweets_section'
        );

        add_settings_field(
            'tweets_theme',
            'Tweets Theme',
            [$this, 'renderThemeInput'],
            'tweets_section',
            'tweets_section'
        );

        add_settings_field(
            'tweets_latitude',
            'Tweets Latitude',
            [$this, 'renderLatitudeInput'],
            'tweets_section',
            'tweets_section'
        );

        add_settings_field(
            'tweets_longitude',
            'Tweets Longitude',
            [$this, 'renderLongitudeInput'],
            'tweets_section',
            'tweets_section'
        );

        add_settings_field(
            'tweets_radius_text',
            'Tweets Radius',
            [$this, 'renderRadiusTextInput'],
            'tweets_section',
            'tweets_section'
        );

        add_settings_field(
            'tweets_range_slider',
            '',
            [$this, 'renderRangeSliderInput'],
            'tweets_section',
            'tweets_section'
        );

        register_setting('tweets_group', 'tweets_theme');
        register_setting('tweets_group', 'tweets_latitude');
        register_setting('tweets_group', 'tweets_longitude');
        register_setting('tweets_group', 'tweets_radius_text');
        register_setting('tweets_group', 'tweets_range_slider');
    }

    public function renderSettingsSection(){
        //echo 'Super-important title';
    }

    public function renderThemeInput(){
        echo '<input type="text" name="tweets_theme" value="'.get_option( 'tweets_theme' ).'">';
    }

    public function renderLatitudeInput(){
        echo '<input class="latitude" type="text" name="tweets_latitude" value="'.get_option( 'tweets_latitude' ).'">';
    }

    public function renderLongitudeInput(){
        echo '<input class="longitude" type="text" name="tweets_longitude" value="'.get_option( 'tweets_longitude' ).'">';
    }

    public function renderRadiusTextInput(){
        echo '<input class="text_radius" type="text" name="tweets_radius_text" value="'.get_option( 'tweets_radius_text' ).'">';
    }

    public function renderRangeSliderInput(){
        echo '<input class="slider_range" type="range" name="tweets_range_slider" min="0" max="100" step="1" value="'.get_option( 'tweets_range_slider' ).'">';
    }


}

$tweets = new TweetsPlugin();
